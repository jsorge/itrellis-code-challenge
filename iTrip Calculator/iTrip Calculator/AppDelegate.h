//
//  AppDelegate.h
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/13/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

