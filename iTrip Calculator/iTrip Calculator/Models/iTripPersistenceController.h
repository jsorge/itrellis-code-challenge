//
//  iTripPersistenceController.h
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/14/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "THPersistenceController.h"

@interface iTripPersistenceController : THPersistenceController
+ (NSString *)modelName;
+ (NSURL *)storeURL;
@end
