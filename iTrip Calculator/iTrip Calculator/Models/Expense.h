//
//  Expense.h
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "THManagedObject.h"

@class Person;

extern NSString *const ExpenseCostKEY;
extern NSString *const ExpensePersonKEY;


@interface Expense : THManagedObject
@property (nonatomic) double cost;
@property (nonatomic, retain) Person *person;
@end
