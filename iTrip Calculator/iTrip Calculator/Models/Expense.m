//
//  Expense.m
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "Expense.h"
#import "Person.h"

NSString *const ExpenseCostKEY = @"cost";
NSString *const ExpensePersonKEY = @"person";

@implementation Expense
@dynamic cost;
@dynamic person;
@end
