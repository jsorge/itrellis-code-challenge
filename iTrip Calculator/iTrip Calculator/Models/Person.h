//
//  Person.h
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "THManagedObject.h"

@class Trip, Expense, NSManagedObject;

extern NSString *const PersonNameKEY;
extern NSString *const PersonTripKEY;
extern NSString *const PersonExpensesKEY;


@interface Person : THManagedObject
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) Trip *trip;
@property (nonatomic, retain) NSSet *expenses;

#pragma mark - API
- (void)addExpenseWithCost:(double)cost description:(NSString *)description;
- (double)totalAmountPaid;
@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addExpensesObject:(Expense *)value;
- (void)removeExpensesObject:(Expense *)value;
- (void)addExpenses:(NSSet *)values;
- (void)removeExpenses:(NSSet *)values;

@end
