//
//  Trip.m
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "Trip.h"
#import "Person.h"

NSString *const TripDestinationKEY = @"destination";
NSString *const TripPeopleKEY = @"people";
NSString *const TripDateKEY = @"date";

@implementation Trip

@dynamic destination;
@dynamic people;
@dynamic date;


#pragma mark - API
- (double)totalTripExpenses;
{
    double total = 0.0;
    for (Person *person in self.people) {
        total += [person totalAmountPaid];
    }
    
    return total;
}

- (double)amountOwedPerPerson;
{
    double tripTotal = [self totalTripExpenses];
    NSInteger numberOfPeople = self.people.count;
    
    return tripTotal / numberOfPeople;
}

- (NSArray *)peopleSortedAlphabetically
{
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:PersonNameKEY ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    return [[self.people allObjects] sortedArrayUsingDescriptors:@[sort]];
}
@end
