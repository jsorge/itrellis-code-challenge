//
//  iTripPersistenceController.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/14/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "iTripPersistenceController.h"
#import "NSFileManager+THExtension.h"

@implementation iTripPersistenceController
+ (NSString *)modelName
{
    return @"iTrip-ManagedObjectModel";
}

+ (NSURL *)storeURL
{
    NSString *documentsPath = [NSFileManager th_documentsPath];
    NSString *storePath = [documentsPath stringByAppendingPathExtension:@"itrip.sqlite"];
    
    return [NSURL fileURLWithPath:storePath];
}
@end
