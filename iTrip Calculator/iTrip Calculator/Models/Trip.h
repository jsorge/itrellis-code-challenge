//
//  Trip.h
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "THManagedObject.h"

@class Person;

extern NSString *const TripDestinationKEY;
extern NSString *const TripPeopleKEY;
extern NSString *const TripDateKEY;


@interface Trip : THManagedObject
@property (nonatomic, retain) NSString * destination;
@property (nonatomic, retain) NSSet *people;
@property (nonatomic, retain) NSDate *date;

#pragma mark - API
- (double)totalTripExpenses;
- (double)amountOwedPerPerson;
- (NSArray *)peopleSortedAlphabetically;
@end

@interface Trip (CoreDataGeneratedAccessors)

- (void)addPeopleObject:(Person *)value;
- (void)removePeopleObject:(Person *)value;
- (void)addPeople:(NSSet *)values;
- (void)removePeople:(NSSet *)values;

@end
