//
//  Person.m
//  
//
//  Created by Jared Sorge on 6/13/15.
//
//

#import "Person.h"
#import "Expense.h"
#import "Trip.h"

NSString *const PersonNameKEY = @"name";
NSString *const PersonTripKEY = @"trip";
NSString *const PersonExpensesKEY = @"expenses";

@implementation Person
@dynamic name;
@dynamic trip;
@dynamic expenses;

#pragma mark - API
- (void)addExpenseWithCost:(double)cost description:(NSString *)description
{
    Expense *expense = [Expense newInstanceInManagedObjectContext:self.managedObjectContext];
    
    expense.cost = cost;
    expense.person = self;
}

- (double)totalAmountPaid
{
    double total = 0;
    
    for (Expense *expense in self.expenses) {
        total += expense.cost;
    }
    
    return total;
}
@end
