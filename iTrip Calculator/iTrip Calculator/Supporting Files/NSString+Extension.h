//
//  NSString+Extension.h
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/16/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extension)
- (BOOL)isNilOrEmpty;
@end
