//
//  NSString+Extension.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/16/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Extension)
- (BOOL)isNilOrEmpty
{
    BOOL isEmpty = NO;
    
    if (self.length == 0) {
        isEmpty = YES;
    } else if (self == nil) {
        isEmpty = YES;
    }
    
    return isEmpty;
}
@end
