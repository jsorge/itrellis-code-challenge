//
//  TripPersonTableViewCell.h
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/16/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Person;

@interface TripPersonTableViewCell : UITableViewCell
#pragma mark - API
- (void)configureWithPerson:(Person *)person balance:(double)balance;
@end
