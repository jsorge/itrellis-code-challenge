//
//  TripPersonTableViewCell.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/16/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "TripPersonTableViewCell.h"
#import "Person.h"

@interface TripPersonTableViewCell ()
@property (nonatomic, strong) Person *person;
@property (nonatomic) double balance;

//IBOutlets
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *paidLabel;
@property (nonatomic, weak) IBOutlet UILabel *balanceLabel;
@end

@implementation TripPersonTableViewCell
#pragma mark - API
- (void)configureWithPerson:(Person *)person balance:(double)balance
{
    self.person = person;
    self.balance = balance;
    
    self.nameLabel.text = person.name;
    self.paidLabel.text = [NSString stringWithFormat:@"%@ Spent", [NSNumberFormatter localizedStringFromNumber:@(person.totalAmountPaid) numberStyle:NSNumberFormatterCurrencyStyle]];
    self.balanceLabel.text = [NSString stringWithFormat:@"%@ Balance", [NSNumberFormatter localizedStringFromNumber:@(balance) numberStyle:NSNumberFormatterCurrencyStyle]];
}
@end
