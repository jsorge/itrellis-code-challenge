//
//  TripDetailViewController.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/14/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "TripDetailViewController.h"
#import "Trip.h"
#import "Person.h"
#import "THTableViewDataSource.h"
#import "TripPersonTableViewCell.h"
#import "PersonDetailTableViewController.h"
#import "NSString+Extension.h"
#import "iTripPersistenceController.h"

@interface TripDetailViewController ()
@property (nonatomic, strong) Trip *trip;
@property (nonatomic, strong) THTableViewDataSource *dataSource;
@property (nonatomic, strong) UITextField *nameField;
@end

static NSString *const PersonCellID = @"PersonCell";
static NSString *const Segue_ShowPersonExpenses = @"segue_showPersonExpenses";

@implementation TripDetailViewController
#pragma mark - API
- (void)configureViewWithTrip:(Trip *)trip
{
    self.trip = trip;
}

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@ Trip", self.trip.destination];
    [self refreshUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshUI];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:Segue_ShowPersonExpenses]) {
        NSAssert([sender isKindOfClass:[Person class]], @"There must be a person sent to the %@ segue", segue.identifier);
        
        PersonDetailTableViewController *destination = segue.destinationViewController;
        [destination configureWithPerson:sender];
    }
}

#pragma mark - Properties
- (THTableViewDataSource *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[THTableViewDataSource alloc] initWithItems:[self.trip peopleSortedAlphabetically]
                                                    cellIdentifier:PersonCellID
                                                configureCellBlock:^(TripPersonTableViewCell *cell, Person *item) {
                                                    double balance = self.trip.amountOwedPerPerson - item.totalAmountPaid;
                                                    [cell configureWithPerson:item balance:balance];
                                                }];
    }
    return _dataSource;
}

#pragma mark - IBActions
- (IBAction)plusButtonTapped:(id)sender
{
    UIAlertController *nameAlert = [UIAlertController alertControllerWithTitle:@"Add Person to the Trip"
                                                                       message:@"Please enter the person's name"
                                                                preferredStyle:UIAlertControllerStyleAlert];
    
    [nameAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        self.nameField = textField;
    }];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         if ([self.nameField.text isNilOrEmpty]) {
                                                             return;
                                                         }
                                                         
                                                         Person *newPerson = [Person newInstanceInManagedObjectContext:self.trip.managedObjectContext];
                                                         newPerson.trip = self.trip;
                                                         newPerson.name = self.nameField.text;
                                                         [[iTripPersistenceController globalPersistenceController] save];
                                                         
                                                         [self refreshUI];
                                                     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel"
                                                           style:UIAlertActionStyleCancel
                                                         handler:nil];
    
    [nameAlert addAction:okAction];
    [nameAlert addAction:cancelAction];
    
    [self presentViewController:nameAlert animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Person *selectedPerson = [self.dataSource itemAtIndexPath:indexPath];
    [self performSegueWithIdentifier:Segue_ShowPersonExpenses sender:selectedPerson];
}

#pragma mark - Private
- (void)refreshUI
{
    self.dataSource = nil;
    self.tableView.dataSource = self.dataSource;
    [self.tableView reloadData];
}
@end
