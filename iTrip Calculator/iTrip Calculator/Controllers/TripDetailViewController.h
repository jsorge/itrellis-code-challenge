//
//  TripDetailViewController.h
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/14/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Trip;

@interface TripDetailViewController : UITableViewController
- (void)configureViewWithTrip:(Trip *)trip;
@end
