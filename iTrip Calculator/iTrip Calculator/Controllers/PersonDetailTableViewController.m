//
//  PersonDetailTableViewController.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/16/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "PersonDetailTableViewController.h"
#import "Person.h"
#import "Expense.h"
#import "THTableViewDataSource.h"
#import "NSString+Extension.h"
#import "iTripPersistenceController.h"

@interface PersonDetailTableViewController () <UITableViewDelegate>
@property (nonatomic, strong) Person *person;
@property (nonatomic, strong) THTableViewDataSource *dataSource;
@property (nonatomic, strong) UITextField *costField;

//IBOutlets
@property (nonatomic,weak) IBOutlet UILabel *totalSpentLabel;
@property (nonatomic, weak) IBOutlet UITableView *tableView;
@end

static NSString *const ExpenceCellID = @"expenseCell";

@implementation PersonDetailTableViewController
- (void)configureWithPerson:(Person *)person
{
    self.person = person;
}

#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = [NSString stringWithFormat:@"%@'s Costs", self.person.name];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshUI];
}

#pragma mark - Properties
- (THTableViewDataSource *)dataSource
{
    if (!_dataSource) {
        _dataSource = [[THTableViewDataSource alloc] initWithItems:[self.person.expenses allObjects]
                                                    cellIdentifier:ExpenceCellID
                                                configureCellBlock:^(UITableViewCell *cell, Expense *item) {
                                                    cell.textLabel.text = [NSNumberFormatter localizedStringFromNumber:@(item.cost) numberStyle:NSNumberFormatterCurrencyStyle];
                                                }];
    }
    return _dataSource;
}

#pragma mark - IBActions
- (IBAction)plusButtonTapped:(id)sender
{
    UIAlertController *addExpenseAlert = [UIAlertController alertControllerWithTitle:@"Add Expense" message:@"How much did the expense cost?" preferredStyle:UIAlertControllerStyleAlert];
    [addExpenseAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        self.costField = textField;
    }];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Cost"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         if ([self.costField.text isNilOrEmpty]) {
                                                             return;
                                                         }
                                                         
                                                         Expense *newExpense = [Expense newInstanceInManagedObjectContext:self.person.managedObjectContext];
                                                         newExpense.cost = [self.costField.text doubleValue];
                                                         newExpense.person = self.person;
                                                         [[iTripPersistenceController globalPersistenceController] save];
                                                         
                                                         [self refreshUI];
                                                     }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [addExpenseAlert addAction:cancelAction];
    [addExpenseAlert addAction:okAction];
    
    [self presentViewController:addExpenseAlert animated:YES completion:nil];
}

#pragma mark - Private
- (void)refreshUI
{
    self.dataSource = nil;
    self.tableView.dataSource = self.dataSource;
    [self.tableView reloadData];
    
    NSString *spentString = [NSNumberFormatter localizedStringFromNumber:@(self.person.totalAmountPaid) numberStyle:NSNumberFormatterCurrencyStyle];
    self.totalSpentLabel.text = [NSString stringWithFormat:@"%@ Total", spentString];
}

@end
