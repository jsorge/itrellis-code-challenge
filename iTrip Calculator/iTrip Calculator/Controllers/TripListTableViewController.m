//
//  TripListTableViewController.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/14/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "TripListTableViewController.h"
#import "THTableViewDataSource.h"
#import "THFetchedResultsControllerHelper.h"
#import "Trip.h"
#import "iTripPersistenceController.h"
#import "TripDetailViewController.h"
#import "NSString+Extension.h"

static NSString *const TripCellID = @"TripCell";
static NSString *const Segue_TripDetail = @"segue_showTripDetail";
static NSString *const Segue_NewTrip = @"segue_newTrip";

@interface TripListTableViewController ()
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) THFetchedResultsControllerHelper *frcDelegate;
@property (nonatomic, strong) THTableViewDataSource *tableViewDataSource;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) UITextField *destinationTextField;
@end

@implementation TripListTableViewController
#pragma mark - Lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.context = [[iTripPersistenceController globalPersistenceController] masterContext];
    
    NSFetchRequest *fetch = [Trip fetchRequest];
    NSSortDescriptor *sortAscending = [NSSortDescriptor sortDescriptorWithKey:TripDateKEY ascending:NO];
    fetch.sortDescriptors = @[sortAscending];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetch
                                                                        managedObjectContext:self.context
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    self.frcDelegate = [[THFetchedResultsControllerHelper alloc] initWithTableView:self.tableView];
    self.fetchedResultsController.delegate = self.frcDelegate;
    
    NSError *fetchError = nil;
    [self.fetchedResultsController performFetch:&fetchError];
    if (fetchError) {
        NSLog(@"Fetch Error:\n%@", fetchError.localizedDescription);
    }
    
    self.tableView.dataSource = self.tableViewDataSource;
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:Segue_TripDetail]) {
        NSAssert([sender isKindOfClass:[Trip class]], @"There must be a trip sent with the %@ segue", Segue_TripDetail);
        
        TripDetailViewController *destination = segue.destinationViewController;
        [destination configureViewWithTrip:sender];
    }
}

#pragma mark - Properties
- (THTableViewDataSource *)tableViewDataSource
{
    if (!_tableViewDataSource) {
        _tableViewDataSource = [[THTableViewDataSource alloc] initWithFetchedResultsController:self.fetchedResultsController
                                                                                cellIdentifier:TripCellID
                                                                            configureCellBlock:^(UITableViewCell *cell, Trip *trip) {
                                                                                cell.textLabel.text = trip.destination;
                                                                                
                                                                                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                                                                                formatter.dateStyle = NSDateFormatterShortStyle;
                                                                                cell.detailTextLabel.text = [formatter stringFromDate:trip.date];
                                                                            }];
    }
    return _tableViewDataSource;
}

#pragma mark - IBActions
- (IBAction)plusButtonTapped:(id)sender
{
    UIAlertController *tripAlert = [UIAlertController alertControllerWithTitle:@"New Trip"
                                                                       message:@"Where is the trip going?"
                                                                preferredStyle:UIAlertControllerStyleAlert];
    [tripAlert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        self.destinationTextField = textField;
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([self.destinationTextField.text isNilOrEmpty]) {
            return;
        }
        
        Trip *newTrip = [Trip newInstanceInManagedObjectContext:self.context];
        newTrip.destination = self.destinationTextField.text;
        newTrip.date = [NSDate date];
        [[iTripPersistenceController globalPersistenceController] save];
        
        self.destinationTextField = nil;
    }];
    
    [tripAlert addAction:cancel];
    [tripAlert addAction:ok];
    
    [self presentViewController:tripAlert animated:YES completion:nil];
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Trip *selectedTrip = [self.tableViewDataSource itemAtIndexPath:indexPath];
    [self performSegueWithIdentifier:Segue_TripDetail sender:selectedTrip];
}

@end
