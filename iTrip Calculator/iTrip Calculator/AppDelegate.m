//
//  AppDelegate.m
//  iTrip Calculator
//
//  Created by Jared Sorge on 6/13/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import "AppDelegate.h"
#import "iTripPersistenceController.h"
@import CoreData;

@interface AppDelegate ()
@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [iTripPersistenceController createGlobalPersistenceControllerWithModelName:[iTripPersistenceController modelName]
                                                                      storeURL:[iTripPersistenceController storeURL]
                                                                     storeType:NSSQLiteStoreType
                                                                      callback:nil];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[iTripPersistenceController globalPersistenceController] save];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[iTripPersistenceController globalPersistenceController] save];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[iTripPersistenceController globalPersistenceController] save];
}
@end
