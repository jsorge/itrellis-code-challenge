//
//  THModalSegue.m
//  TaphouseKit
//
//  Created by Jared Sorge on 2/12/15.
//  Copyright (c) 2015 Taphouse Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THModalSegue : UIStoryboardSegue

@end
