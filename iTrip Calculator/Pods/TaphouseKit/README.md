# TaphouseKit

[![CI Status](http://img.shields.io/travis/Jared Sorge/TaphouseKit.svg?style=flat)](https://travis-ci.org/Jared Sorge/TaphouseKit)
[![Version](https://img.shields.io/cocoapods/v/TaphouseKit.svg?style=flat)](http://cocoadocs.org/docsets/TaphouseKit)
[![License](https://img.shields.io/cocoapods/l/TaphouseKit.svg?style=flat)](http://cocoadocs.org/docsets/TaphouseKit)
[![Platform](https://img.shields.io/cocoapods/p/TaphouseKit.svg?style=flat)](http://cocoadocs.org/docsets/TaphouseKit)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TaphouseKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "TaphouseKit"

## Author

Jared Sorge, jared@jsorge.net

## License

TaphouseKit is available under the MIT license. See the LICENSE file for more info.

