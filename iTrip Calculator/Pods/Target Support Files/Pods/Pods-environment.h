
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// TaphouseKit
#define COCOAPODS_POD_AVAILABLE_TaphouseKit
#define COCOAPODS_VERSION_MAJOR_TaphouseKit 0
#define COCOAPODS_VERSION_MINOR_TaphouseKit 3
#define COCOAPODS_VERSION_PATCH_TaphouseKit 2

